﻿using System;
using System.Collections.Generic;
	class Program
	{
		static void Main(string[] args)
		{

			Dictionary<string, string> dayList = new Dictionary<string, string>();

			dayList.Add("Monday", "Weekday");
			dayList.Add("Tuesday", "Weekday");
			dayList.Add("Wednesday", "Weekday");
			dayList.Add("Thursday", "Weekday");
			dayList.Add("Friday", "Weekday");
			dayList.Add("Saturday", "Weekend");
			dayList.Add("Sunday", "Weekend");

			// Read all data
			Console.WriteLine("dayList");

			foreach (KeyValuePair<string, string> day1 in dayList)
			{
				Console.WriteLine("Key = {0}, Value = {1}",
			                      day1.Key, day1.Value);
			}

			Console.ReadKey();

		}
	}

