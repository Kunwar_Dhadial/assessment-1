﻿using System;

namespace Task5
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("24 Hour Time Format : " + DateTime.Now.ToString("HH:mm ") + " ");
		
			Console.WriteLine("12 Hour Time Format : " + DateTime.Now.ToString("hh:mm tt") + " ");
		}
	}
}
