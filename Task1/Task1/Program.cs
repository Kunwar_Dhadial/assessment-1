﻿using System;

namespace Task1
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			var name = " ";
			var age = 0;

			Console.WriteLine("Hello! What is your name?");
			name = (Console.ReadLine());
			Console.WriteLine("How old are you?");
			age = int.Parse(Console.ReadLine());

			Console.WriteLine("Your name is " + name + " and you are " + age + " years old.");
			Console.WriteLine("Your name is {0} and you are {1} years old.", name, age);
			Console.WriteLine($"Your name is {name} and you are {age} years old.");

		}
	}
}
