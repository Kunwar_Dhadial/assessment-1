﻿using System;

namespace Task33
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			var students = 0;
			var groups = 0;

			Console.WriteLine("Enter the total number of students=");
			students = int.Parse(Console.ReadLine());
			groups = students / 28;
			//as each tutorial group is of 28 studentss

			Console.WriteLine($"Number of tutorial groups={groups}");
		}
	}
}
