﻿using System;
using System.Collections.Generic;

namespace DictionarySample
{
	class Program
	{
		static void Main(string[] args)
		{
			var fruits = 0;
			var vegetables = 0;

			Dictionary<string, string> vegList = new Dictionary<string, string>();

			vegList.Add("Apple", "Fruit");
			vegList.Add("Potato", "Veg");
			vegList.Add("Mango", "Fruit");
			vegList.Add("Spinach", "Veg");
			vegList.Add("Banana", "Fruit");

			// Read all data
			Console.WriteLine("Fruit List");


			foreach (KeyValuePair<string, string> veg1 in vegList)
			{
				//Console.WriteLine("Key = {0}, Value = {1} veg1.Key, veg1.Value);
				if (veg1.Value == "Fruit")
				{
					Console.WriteLine("Key = {0}, Value = {1}",
								  veg1.Key, veg1.Value);
					fruits++;
				}
				else
					vegetables++;


			}
			Console.WriteLine($"Number of fruits= {fruits}");
			Console.WriteLine($"Number of vegetables= {vegetables}");

		}
	}
}
