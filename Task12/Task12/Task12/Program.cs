﻿using System;

namespace Task12
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			
			var x= 0;
			Console.WriteLine("Enter an even number");
			x = int.Parse(Console.ReadLine());



			if (x % 2 == 0)
			{
				Console.WriteLine($"Yes {x} is even number ");
			}
			else
			{
				Console.WriteLine($"No {x} is not even number ");
			}
		}
	}
}
