﻿using System;

namespace Task35
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			int a, b, c;
			int eq1, eq2, eq3;
			Console.WriteLine("Enter any 3 numbers");

			a = int.Parse(Console.ReadLine());
			b = int.Parse(Console.ReadLine());
			c = int.Parse(Console.ReadLine());

			eq1 = a + b + c;
			eq2 = a * b * c;
			eq3 = a - b - c;

			Console.WriteLine($"Equation 1 = {eq1}");
			Console.WriteLine($"Equation 2 = {eq2}");
			Console.WriteLine($"Equation 3 = {eq3}");
		}
	}
}
