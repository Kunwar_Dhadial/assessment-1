﻿using System;

namespace Task28
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Enter any 3 words");

			string s = Console.ReadLine();
			// Split string on spaces.
			// ... This will separate all the words.
			string[] words = s.Split(' ');
			foreach (string word in words)
			{
				Console.WriteLine(word);

			}
		}
	}
}
