﻿using System;

namespace Task25
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			int a;
			do
			{
				// repeat until input is a number
				Console.WriteLine("Please enter a number! ");
			} while (!int.TryParse(Console.ReadLine(), out a));

			Console.WriteLine("You entered {0}", a);

		}
	}
}
