﻿using System;

namespace Task9
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			var x = 2016;

			for (int i = 1; i <= 20; i++)
				{
				if (x % 4 == 0) //Checking weather the year is divisible by 4 or not(because divisible by 4 are leap years)
				{
					Console.WriteLine(i+ " Leap year = " +x);
				}
				x++;
			}
		}
	}
}
