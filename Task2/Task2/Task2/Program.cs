﻿using System;

namespace Task2
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			var month = " ";
			var day = 0;

			Console.WriteLine("In which month you were born?");
			month = (Console.ReadLine());

			Console.WriteLine("On which day?");
			day = int.Parse(Console.ReadLine());

			Console.WriteLine($"You were born on {day} {month}.");

		}
	}
}
