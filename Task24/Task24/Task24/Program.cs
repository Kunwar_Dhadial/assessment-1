﻿using System;

namespace Task24
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			string op="";
			do
			{
				Console.WriteLine("Menu");
				Console.WriteLine("1. Option One");
				Console.WriteLine("2. Option Two");
				Console.WriteLine("3. Option Three");
				Console.WriteLine("4. Option Four");
				Console.WriteLine("Select to clear");

				Console.ReadLine();
				Console.Clear();
				Console.WriteLine("Press m/M to go back to main menu");
				 op = Console.ReadLine();
			} while (op == "M" || op == "m");
		}
	}
}
