﻿using System;

class Program
{
	static void Main()
	{
		string[] colors = new string[]
		{
		"Red",
		"Blue",
		"Yellow",
		"Green",
		"Pink"
		};
		// Called Array.Sort method.
		Array.Sort(colors);
		Array.Reverse(colors);


		foreach (string color in colors)
		{
			Console.WriteLine(color);
		}
	}
}

