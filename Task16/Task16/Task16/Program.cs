﻿using System;

namespace Task16
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			char[] str;
			Console.WriteLine("Enter a string");
			str = Console.ReadLine().ToCharArray();
			int l = str.Length;
			Console.WriteLine($"Number of characters are {l}");

		}
	}
}
