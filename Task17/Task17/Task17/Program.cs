﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task17
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			var plist = new List<Tuple<int, string>>();
			plist.Add(new Tuple<int, string>(22, "Tony"));
			plist.Add(new Tuple<int, string>(24, "Den"));
			plist.Add(new Tuple<int, string>(21, "Ron"));

			foreach (var person in plist)
			{
				int age = person.Item1;
				string name = person.Item2;

				Console.WriteLine($"Name={name} Age={age}");
			}
		}
	}
}
