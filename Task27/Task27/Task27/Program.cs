﻿using System;

namespace Task27
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			string[] colors = new string[]
	{
		"Red",
		"Blue",
		"Yellow",
		"Green",
		"Pink"
			};
			// Called Array.Sort method.
			Array.Sort(colors);

			foreach (string color in colors)
			{
				Console.WriteLine(color);
			}
		}
	}
}
