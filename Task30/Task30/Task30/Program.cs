﻿using System;

namespace Task30
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			var x= 0;
			var y= 0;
			var result = 0;
			Console.WriteLine("Enter the first number to be added: ");
			x = int.Parse(Console.ReadLine());
			Console.WriteLine(" Enter the second number to be added: ");
			y = int.Parse(Console.ReadLine());
			result = x + y;
			Console.WriteLine("The sum of two numbers is: " + result);

			string x1;
			string y1;
			string result1;
			Console.WriteLine("Enter the first number to be added: ");
			x1 = Console.ReadLine();
			Console.WriteLine("Enter the second number to be added: ");
			y1 = Console.ReadLine();
			result1 = x1 + y1;
			Console.WriteLine("The sum of two string numbers is: " + result1);

			Console.ReadLine();


		}
	}
}
